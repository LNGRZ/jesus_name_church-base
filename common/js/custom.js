// IE support for 'main'
document.createElement('main');

// Object-Fit
$(function () { objectFitImages() });

// ScrollTop
$(document).ready(function($) {
  $(window).scroll(function(){
    var scroll = $(window).scrollTop();
    if (scroll >= 10) {
      $('.site-header.fr-pg').addClass('show');
      $('.site-atf.fr-pg').addClass('hide');
    } 
    else {
      $('.site-header.fr-pg').removeClass('show');
      // $('.site-atf.fr-pg').removeClass('hide');
    }
  });
});

// Accordion
$(document).ready(function($) {
  var acc = document.getElementsByClassName('accordion');
  var i;
  for (i = 0; i < acc.length; i++) {
    acc[i].addEventListener('click', function() {
      this.classList.toggle('active');
      var panel = this.nextElementSibling;
      if (panel.style.maxHeight){
        panel.style.maxHeight = null;
      } else {
        panel.style.maxHeight = panel.scrollHeight + 'px';
      } 
    });
  };
});

// Toggle
$(document).ready(function($) {
  $('.menu-button').click(function() {
    $('body').stop().toggleClass('modal-open');
    $('.site-navigation').stop().toggleClass('open');
  });

  $("#view-main, #view-atf").click(function(){
    $('.site-atf.fr-pg').stop().toggleClass('hide');
  });
});

// Disable mouse wheel
$(document).ready(function($) {
  if ($(window).width() > 992) {
    $('.site-atf.fr-pg').bind('mousewheel', function() {
      return false;
    });
  }
});

// Smooth scroll
$(document).ready(function($) {
  $('a[href^="#"]').on('click', function(event) {
    var target = $(this.getAttribute('href'));
    if( target.length ) {
      event.preventDefault();
      $('html, body').stop().animate({
        scrollTop: target.offset().top
      }, 200);
    }
  });
});

// Detect if user is using TAB to navigate
function handleFirstTab(e) {
  if (e.keyCode === 9) { // the 'I am a keyboard user' key
    document.body.classList.add('tab-used');
    window.removeEventListener('keydown', handleFirstTab);
  }
}
window.addEventListener('keydown', handleFirstTab);
